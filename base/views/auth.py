from django.conf import settings
from django.core import serializers
from django.shortcuts import render
from django.contrib.auth import logout
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from base.models  import *
import json
from django.http import HttpResponse
import base64
import hashlib
from django.core.files import File
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.files.base import ContentFile
import uuid
import time
import requests
import smtplib
from email.mime.text import MIMEText







def registration(request):
    try:
        request.session["session_id"]
        return HttpResponseRedirect('/base/home')

    except:
        if request.method=="GET":
            return render(request, 'base/registration.html')
        if request.method=="POST":

            user_name = request.POST['user_name']
            user_email = request.POST['user_email']
            user_password = request.POST['user_password']
            user_img = request.POST["base64_user_img"]
            encrypted_pass=hashlib.sha256(user_password.encode()).hexdigest()

            # url = 'http://157.7.242.91:8010/api/registration_with_email/'
            url = ''+settings.API_SERVER+'/api/registration/'


            params = {'username': user_name,
                      'password': encrypted_pass,
                      'email': user_email,
                      'profile_image': user_img,
                      'confirm_email_url':""+settings.WEB_SERVER+"/base/confirm_email/"
                }
            r = requests.post(url, data=params)
            result = r.json()


            error_message=[]
            try:
                for i in result['data']['email']:
                    error_message.append('Email- '+i)
            except:
                pass

            try:
                for i in result['data']['username']:
                    error_message.append('Username- '+i)
            except:
                pass

            try:
                for i in result['data']['profile_image']:
                    error_message.append(i)
            except:
                pass

            try:
                for i in result['data']['message']:
                    error_message.append(i)
            except:
                pass



            if result["status"]=="success":

                return render(request, 'base/login.html', {"message": result['message']})

            elif result['status']=='error':
                return render(request, 'base/registration.html', {'error_message': error_message})

            else:
                return render(request, 'base/registration.html', {'error_message': 'Unknown Error.'})






def confirm_email(request,security_code):

    try:
        url = ''+settings.API_SERVER+'/api/api_confirm_email/0/'+security_code+''

        r = requests.get(url)
        result = r.json()

        print(result)

        return render(request, 'base/login.html', {"message": result['message']})
    except:
        return HttpResponse("Exception occures")


def login(request):
    try:

        request.session["session_id"]

        return HttpResponseRedirect('/base/home')

    except:

        if request.method=="GET":
            return render(request, 'base/login.html')

        if request.method=="POST":

            user_email = request.POST['email']
            user_password = request.POST['password']

            encrypted_pass = hashlib.sha256(user_password.encode()).hexdigest()


            url = ''+settings.API_SERVER+'/api/login/'
            params = { 'password': encrypted_pass, 'email': user_email}
            r = requests.post(url, data=params)
            result = r.json()


            if result["status"]=="error":
                return render(request, 'base/login.html', {'error_message':result["data"]["error_message"]})

            elif result["status"]=="success":

                request.session.modified = True
                request.session["session_id"] = result["data"]["session"]
                request.session["user_img"] = result["data"]["profile_image"]
                request.session["user_id"] = result["data"]["id"]
                return HttpResponseRedirect('/base/home')
            else:
                return render(request, 'base/login.html', {'error_message': 'Unknown Error'})



def logout_user(request):

    try:
        session = request.session['session_id']
        url = '' + settings.API_SERVER + '/api/logout_user/'
        params = {'session': session}
        r = requests.get(url, params=params)
        result = r.json()

        print(result)

        if result["status"] == "success":
            del request.session['session_id']
            del request.session['user_img']
            del request.session['user_id']

            return render(request, 'base/login.html', {"message": result["message"]})
        if result["status"] == "error":
            return render(request, 'base/login.html', {"error_message": result["message"]})

        return render(request, 'base/login.html')

    except:
        return render(request, 'base/login.html')


