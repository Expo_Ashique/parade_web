from django.conf import settings
from django.core import serializers
from django.shortcuts import render
from django.contrib.auth import logout
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from base.models  import *
import json
from django.http import HttpResponse
import base64
import hashlib
from django.core.files import File
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.files.base import ContentFile
import uuid
import time
import requests
import smtplib
from email.mime.text import MIMEText
from django.shortcuts import redirect



def home(request):

    session=request.session.get("session_id",None)
    # category=request.GET.get('category',0)

    url = ''+settings.API_SERVER+'/api/timeline/'

    if session == None:
        params= {}
    else:
        params = {
            "session": session,
            }

    r = requests.get(url, params=params)
    result = r.json()

    url2 = settings.API_SERVER+'/api/get_category_list/'
    params2 = {}
    r2 = requests.get(url2, params=params2)
    result2 = r2.json()



    if result['status']=="success":

        return render(request, 'base/home.html', {"timeline_data": result['data']['timeline_data'],
                                              "profile_info":result['data']['profile_info'],
                                              "categories":result2
                                              })
    else:
        return render(request, 'base/home.html', {"timeline_data": None,
                                                  "profile_info": None,
                                                  "categories": settings.CATEGORIES
                                                  })



def Ajax_load_category_posts(request):
    try:
        session = request.session.get("session_id",None)
        category_id=request.GET.get('category_id',None)


        url = '' + settings.API_SERVER + '/api/timeline/'
        params = {'session': session, 'category_id': category_id}
        r = requests.get(url, params=params)
        result = r.json()

        # print(result)
        return HttpResponse(json.dumps(result['data']))
    except:
        return HttpResponse('0')

















def Ajax_favorite(request):
    session = request.session.get("session_id",None)

    if session== None:

        return HttpResponse("-1")
    try:

        post_id=request.POST['post_id']
        isFavorite=request.POST['isFavorite']



        url = ''+settings.API_SERVER+'/api/favorite/'
        params = {'session': session, 'post_id': post_id,'is_favorite':isFavorite}
        r = requests.post(url, data = params)
        result = r.json()

        return HttpResponse("1")
    except:
        return HttpResponse("0")


def Ajax_follow(request):
    session = request.session.get("session_id", None)

    if session == None:
        return HttpResponse("-2")
    try:

        user_id=request.POST['user_id']
        status=request.POST['status']

        url = ''+settings.API_SERVER+'/api/follower/'
        params = {'session': session, 'following_user_id': user_id,'is_follow':status}
        r = requests.post(url, data = params)
        result = r.json()

        print(result)

        if result['status']=='success':
            return HttpResponse("1")
        elif result['status']=='exception':
            return HttpResponse("-1")

    except:
        return HttpResponse("0")



def profile(request,user_id):

    # print("id ===>>   ",user_id)


    session=request.session.get("session_id",None)

    url = ''+settings.API_SERVER+'/api/timeline/'
    params = {'session': session,'user_id':user_id}
    r = requests.get(url, params=params)
    result = r.json()
    print(result)



    return render(request, 'base/profile.html', {
        "timeline_data": result['data']['timeline_data'],
        "profile_info": result['data']['profile_info'],
                    })

