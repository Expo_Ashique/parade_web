from django.conf.urls import url
from django.http import HttpResponseRedirect
from . import views
from django.conf import settings
from django.conf.urls.static import static

from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [

    url(r'^$', lambda r: HttpResponseRedirect('home')),
    url(r'^home/$', views.home, name="home"),   #timeline.py
    url(r'^profile/(?P<user_id>.*)/$', views.profile, name="profile"),
    url(r'^registration/$', views.registration, name="registration"),
    url(r'^confirm_email/(?P<security_code>.*)/$', views.confirm_email, name="confirm_email"),
    url(r'^login/$', views.login, name="login"),
    url(r'^logout_user/$', views.logout_user, name="logout_user"),
    url(r'^Ajax_favorite/$', views.Ajax_favorite, name="Ajax_favorite"),
    url(r'^Ajax_load_category_posts/$', views.Ajax_load_category_posts, name="Ajax_load_category_posts"),
    url(r'^card/$', views.card, name="card"),
    url(r'^Ajax_post_cardlist/$', views.Ajax_post_cardlist, name="Ajax_post_cardlist"),
    url(r'^Ajax_follow/$', views.Ajax_follow, name="Ajax_follow"),
    url(r'^single_post/(?P<post_id>.*)/$', views.single_post, name="single_post"),
    url(r'^ajax-get-url/$', views.getUrlByAjax, name="getUrlByAjax"),


]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += staticfiles_urlpatterns()