(function(){
    var  cardSlider =function(options){
        var that={};
        that.items = options.items;
        that.controllers = options.controllers,
        that.currentItem = parseInt(options.currentItem);
        that.controller = options.controller;
        that.prevItem =options.prevItem,
        that.captions = options.captions,
        that.prevBtn = options.prevBtn,
        that.nextBtn = options.nextBtn;

        that.start = function(){

            that.remove();

            that.controllers[that.currentItem].classList.add('active');
            that.items[that.currentItem].classList.add('active');
            that.captions[that.currentItem].classList.add('active');

            that.update();
        }

        that.remove = function(){
            for(var i=0; i<that.items.length; i++){
                that.items[i].classList.remove('active');
                that.controllers[i].classList.remove('active');
                that.captions[i].classList.remove('active');
            }
        }

        that.update= function(){
            if(that.currentItem>0){
                that.prevBtn.disabled  = false;
                that.prevBtn.setAttribute('data-prevItem', that.currentItem-1);
            }
            else{
                that.prevBtn.disabled  = true;
                that.prevBtn.setAttribute('data-prevItem', '');
            }
            console.log(that.currentItem);
            if(that.currentItem<that.items.length-1){
                that.nextBtn.disabled  = false;
                that.nextBtn.setAttribute('data-nextItem', that.currentItem+1);
            }
            else{
                that.nextBtn.disabled  = true;
                that.nextBtn.setAttribute('data-nextItem', '');
            }
        }

        return that;
    }


    var controllerList = document.querySelectorAll('.custom-slider .items .control');

    prevBtn = document.getElementById('prev-slider-btn');
    nextBtn = document.getElementById('next-slider-btn');


    //console.log(ControllerList.length);
    var initialize = function() {
        var currentItem = this.getAttribute("data-item");
        var slider = cardSlider({
            items: document.querySelectorAll('.custom-slider .items .item'),
            controllers:controllerList,
            currentItem: currentItem,
            controller : this,
            captions: document.querySelectorAll('.slider-caption-container .item-caption'),
            nextBtn:nextBtn,
            prevBtn:prevBtn
        })
        slider.start();
    };



    for (var j = 0; j< controllerList.length; j++) {
         controllerList[j].addEventListener('click', initialize, false);
    }


    prevBtn.addEventListener('click', function(){
        var currentItem = this.getAttribute("data-prevItem");
        var slider = cardSlider({
            items: document.querySelectorAll('.custom-slider .items .item'),
            controllers:controllerList,
            currentItem: currentItem,
            controller : this,
            captions: document.querySelectorAll('.slider-caption-container .item-caption'),
            nextBtn:nextBtn,
            prevBtn:prevBtn
        })
        slider.start();
    } );


    nextBtn.addEventListener('click', function(){
        var currentItem = this.getAttribute("data-nextItem");
        var slider = cardSlider({
            items: document.querySelectorAll('.custom-slider .items .item'),
            controllers:controllerList,
            currentItem: currentItem,
            controller : this,
            captions: document.querySelectorAll('.slider-caption-container .item-caption'),
            nextBtn:nextBtn,
            prevBtn:prevBtn
        })
        slider.start();
    })

}());